## Starting Development

Start the app in the `dev` environment. This starts the renderer process in [**hot-module-replacement**](https://webpack.js.org/guides/hmr-react/) mode and starts a webpack dev server that sends hot updates to the renderer process:

```bash
$ npm run dev
```

## Packaging for Production

To package apps for the local platform:

```bash
$ npm run package
```

## Docs

See more [docs and guides here](https://electron-react-boilerplate.js.org/docs/installation)
