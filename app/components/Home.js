/* eslint-disable import/order */
/* eslint-disable prettier/prettier */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/button-has-type */
/* eslint-disable class-methods-use-this */
/* eslint-disable func-names */
import React, { Component } from 'react';
import './Home.css';

const { startTezsterNodes } = require('../constants/tezster-script');

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRunningCli: false
    };
    this.handleTezsureCLI = this.handleTezsureCLI.bind(this);
  }

  handleTezsureCLI() {
    startTezsterNodes((err, result) => {
      if (err) {
        console.log('==== err ====>>>>>', err);
      }
      console.log('==== result ====>>>>', result);
    });
  }

  render() {
    return (
      <div>
        Tezsure
        <br />
        <button onClick={this.handleTezsureCLI}>Run Script</button>
      </div>
    );
  }
}

export default Home;
