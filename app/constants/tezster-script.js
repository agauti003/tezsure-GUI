/* eslint-disable no-new */
/* eslint-disable no-unused-vars */
const childProcess = require('child_process');

module.exports = {
  startTezsterNodes(callback) {
    const script =
      'cd ~/Documents/www/html/tezsure/electron-react-boilerplate/Tezster-CLI/ && sudo tezster start-nodes && sudo lsof -i :18731';
    const runShellScript = (shellScript, clbk) => {
      childProcess.exec(shellScript, (err, stdout) => {
        if (err) {
          clbk(err, null);
        }
        clbk(null, stdout);
      });
    };
    runShellScript(script, (err, result) => {
      if (err) {
        callback(err, null);
      }
      callback(null, result);
    });
  }
};
